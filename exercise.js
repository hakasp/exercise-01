// Write two binary functions - add and mul, which take two numbers and return their sum and product respectively

function add(x, y) {
	return x + y;
}
console.log(add(5, 7)); // 12

function mul(x, y) {
	return x * y;
}
console.log(mul(5, 7)); // 35